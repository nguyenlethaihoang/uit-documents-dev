import React, { useEffect, useState } from "react";
import {
    Link
} from "react-router-dom";

import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import PendingActionsIcon from '@mui/icons-material/PendingActions';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import './Header.css';
import logo from '../../assets/logo.png';
import { useAuth } from "../../hooks/useAuth";

//
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';

const options = [
    'Thông báo 1',
    'Thông báo 2',
    'Thông báo 3',
    'Thông báo 4',
    'Thông báo 5',
    'Thông báo 6',
    'Thông báo 7',
];

const ITEM_HEIGHT = 48;

export default function Header() {
    const { user, logout } = useAuth();

    //
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const authSection = user
        ? <>
            <li className="hover">
                <Link to="/user-profile">{user.email}</Link>
            </li>
            <li className="hover">
                <button onClick={logout}>Đăng xuất</button>
            </li>
        </>
        : <>
            <li className="hover">
                <Link to="/sign-in">Đăng nhập</Link>
            </li>
            <li className="border-l-2 hover">
                <Link to="/sign-up">Đăng ký</Link>
            </li>
        </>

    return (
        <nav className="bg-header nav shadow-md ">
            <div className="text-white font-extrabold flex items-center hover">
                <Link to="/" className="text-xl flex items-center">
                    KH <img src={logo} alt="logo" width={28} height={28} className="mr-1" /> TÀI LIỆU UIT</Link>

            </div>
            <ul className="flex items-center">
                {user?.email === "admin" ?
                    <li className="hover">
                        <Link to="/approval">
                            <PendingActionsIcon />
                        </Link>
                    </li> : <></>}

                <li className="hover">
                    <NotificationsActiveIcon
                        aria-label="more"
                        id="long-button"
                        aria-controls={open ? 'long-menu' : undefined}
                        aria-expanded={open ? 'true' : undefined}
                        aria-haspopup="true"
                        onClick={handleClick}
                    >
                        <MoreVertIcon />
                    </NotificationsActiveIcon>
                    <Menu
                        id="long-menu"
                        MenuListProps={{
                            'aria-labelledby': 'long-button',
                        }}
                        anchorEl={anchorEl}
                        open={open}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        onClose={handleClose}
                        PaperProps={{
                            style: {
                                maxHeight: ITEM_HEIGHT * 4.5,
                                width: '300px',
                                left: '400px',
                            },
                        }}
                    >
                        <div className="text-xl font-bold pl-3">Thông báo</div>
                        {options.map((option) => (
                            <MenuItem key={option} selected={option === 'None'} onClick={handleClose} >
                                {option}
                            </MenuItem>

                        ))}
                    </Menu>
                    {/* <NotificationsActiveIcon /> */}
                </li>
                <li className="hover">
                    <Link to="/upload">
                        <button className="upload">Tải lên
                            <FileUploadIcon sx={{ fontSize: 20 }} /></button>
                    </Link>

                </li>

                {authSection}

            </ul>
        </nav>
    );
}