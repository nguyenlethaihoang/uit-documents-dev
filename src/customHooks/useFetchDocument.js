import { useEffect, useRef, useState } from "react";
import documentApi from "../api/documentApi";


const useFetchDocument = () => {
    const [documentList, setDocumentList] = useState([]);
    useEffect(() => 
    {
        const fetchDocumentList = async () => {
            const response = await documentApi.getAll();
            setDocumentList(response.rows) 
        }
        fetchDocumentList();
    }, [])
    return documentList;
}

export default useFetchDocument

