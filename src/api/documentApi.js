import axiosClient from "./axiosClient";

const documentApi = {
    getAll: (params) => {
        const url = '/documents';
        return axiosClient.get(url, { params });
    },
    get: (id) => {
        const url = `/storage/get_account_officer/${id}`;
        return axiosClient.get(url);
    },

}
    export default documentApi;