// Libraries
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import * as _ from 'lodash';
// Components
import SideBar from "../../components/SideBar/SideBar";
import './Documents.css';
// Data
import { subjects } from "../../Data";
import { documentType } from "../../data/document";
// icon
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import VisibilityIcon from '@mui/icons-material/Visibility';
import InfoIcon from '@mui/icons-material/Info';

// custom Hook
import useFetchDocument from "../../customHooks/useFetchDocument";
import { useLocation } from "react-router-dom";


// ---------- MAIN ----------

function Documents() {
    const documentList = useFetchDocument();
    const [documents, setDocuments] = useState([]);
    //  Fetch Data
    useEffect(() => {
        const fetchDataDocument = async () => {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/documents/`);
            const data = await response.json()
            setDocuments(data)
            
        };
        fetchDataDocument();
    }, []);

    const [newSubject, setNewSubject] = useState([]);
    useEffect(() => {
        const fetchDataSubject = async () => {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/subjects/`);
            const data = await response.json()
            setNewSubject(data)
            
        };
        fetchDataSubject();
        // console.log(newSubject);
        
    }, []);

    let location = useLocation();
    const subjectId = location.pathname.split('/').reverse()[0];
    // console.log(subjectId);
    console.log(newSubject);


    // let { subjectId } = useParams();
    const [subject, setSubject] = useState([]);

    useEffect(() => {
        const subj = newSubject.find((subject) => subject.id === subjectId);
        setSubject(subj);
    }, []);

    // function myFunction() {
    //     documents.getElementById("myDropdown").classList.toggle("show");
    //   }
    
    const renderDocs = () => {
        return documentType.map((type, index) => {
        {
        return documentType.length > 0
            ? <div key={index} className="doc-items">
                <h3 className="text-xl font-medium">{type.name}</h3>
                <ul>
                    {documents.map((doc) => {
                        if (type.id == doc.type && doc.subjectID == subjectId) 
                        return (
                            <li className="doc-item" key={doc.id}>
                                <div>{doc.name + " - " + doc.size}</div>
                                <div className="flex">
                                    <div className="mx-1">{doc.date}</div>
                                    
                                    <button className="mx-1" >
                                        <a href={doc.file} target="_blank"><FileDownloadIcon /></a>
                                    </button>

                                    {/* <div class="dropdown"> */}
                                        <button onclick="myFunction()" className="dropbtn mx-1">
                                            <VisibilityIcon />
                                        </button>
                                        {/* <div id="myDropdown" className="dropdown-content">
                                            <a href="#home">Người đăng: Nguyễn Thị Hoài Thương</a>
                                        </div> */}
                                    {/* </div> */}

                                    <button className="mx-1">
                                        <InfoIcon />
                                    </button>


                                </div>
                            </li>
                        )
                    })}
                </ul>
            </div>
            : <></>
        }
        })
    }

    return (
        <div>
            <div className="bg-subject">
                <img
                    className="bg-subj"
                    src={newSubject[subjectId-1]?.imgUrl}
                    alt="subject"
                />
                <div className="bg-subject-blur"></div>
                <div className="z-40 text-white text-4xl">{newSubject[subjectId-1]?.name}</div>
            </div>

            <section className="flex" >
                <SideBar />

                <div className="article">
                    <div className="new-doc"></div>
                    {renderDocs()}

                </div>
            </section>
        </div>
    );
}
export default Documents;

