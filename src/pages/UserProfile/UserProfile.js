import React, { useEffect, useRef, useState } from "react";
import './UserProfile.css';
import avatar from '../../assets/avatar.png';
import AddAPhotoIcon from '@mui/icons-material/AddAPhoto';
import { useAuth } from "../../hooks/useAuth";
import { useNavigate } from "react-router-dom";


function UserProfile() {
  const { user, logout } = useAuth(); 
  const [displayUser, setDisplayUser] = useState("");
  const defaultAvatar = '/assets/avatar.png';

  useEffect(() => {
    const fetchDataDocument = async () => {
        const response = await fetch('http://103.75.185.190:4444/users/');
        const users = await response.json()

        const u = users.filter(u => u.userID == user.email);
        setDisplayUser(u);
    };
    fetchDataDocument();
}, []);
  

  const [src, setSrc] = useState(avatar);

  const onImagePicked = (img) => {
    setSrc(URL.createObjectURL(img))
  }

  const inputRef = useRef(null);

  return (
    <div className="h-screen mt-12 w-2/3 m-auto">
      <div className="flex" >
        <div className="flex flex-column pr-14 border-r-2">
          <div className="relative">
            <img
              className="avatar"
              src={src} alt="avatar" />

            <button
              onClick={() => inputRef.current.click()}
              className="btn-add-avatar">
              <AddAPhotoIcon />
            </button>
          </div>

          <input type="file"
            className="hidden"
            ref={inputRef}
            multiple={false}
            onChange={e => onImagePicked(e.target.files[0])}
            id="avatar" name="avatar"
            accept="image/png, image/jpeg" >
          </input>
          
        </div>

        <div>
          <h1 className="title-user-infor">Thông tin tài khoản</h1>
          <form className="form-user-infor">
            <div className="item-user-infor">
              <label className="label-user-infor">
                Tên hiển thị
              </label>
              <input
                type="text"
                name="name"
                placeholder="Nguyễn Thị Hoài Thương"
                value={user.email}
                className="input-user-infor"
              ></input>
            </div>
            <div className="item-user-infor">
              <label className="label-user-infor">
                Email
              </label>
              <input
                type="text"
                className="input-user-infor"
                value={user.email + "@gm.uit.edu.vn"}
                placeholder="19521010@gm.uit.edu.vn"
              ></input>
            </div>
            {/* <div className="item-user-infor">
              <label className="label-user-infor">
                Mã số sinh viên
              </label>
              <input
                type="text"
                className="input-user-infor"
                value={displayUser.studentID}
                placeholder="19521010"
              ></input>
            </div> */}
            <div className="item-user-infor">
              <label className="label-user-infor">
                Ngày sinh
              </label>
              <input
                type="text"
                className="input-user-infor"
                value={displayUser.dateOfBirth}
                placeholder="Đang cập nhật"
              ></input>
            </div>


          </form>

        </div>


      </div>
    </div>
  );
}
export default UserProfile;